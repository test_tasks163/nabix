import {Date} from './date';
 
export class DataService{
 
    private data: Date[] = [
		{date: "00.00.0000"}
    ];
    getData(): Date[] {
         
        return this.data;
    }
    addData(date: string){
		this.data.splice(0);
        this.data.push(new Date(date));
    }
	
}