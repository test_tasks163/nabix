import { Component, OnInit } from '@angular/core';
import {DataService} from './data.service';
import {Date} from './date';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [DataService]
})
export class AppComponent implements OnInit { 
     
    items: Date[] = [];
	selected:string;
    constructor(private dataService: DataService){}
     
    addItem(date: string){
         
        this.dataService.addData(date);
    }
    ngOnInit(){
        this.items = this.dataService.getData();
    }
	showSelected(item){
        this.selected = item;
		console.log(item);
    }
}
